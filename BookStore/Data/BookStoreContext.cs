﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Data
{
    class BookStoreContext:DbContext
    {
        public DbSet<Book> Books { get; set; }        

        public BookStoreContext():base("localdb")
        {
            
        }
    }
}
